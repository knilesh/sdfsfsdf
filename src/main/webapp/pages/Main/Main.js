Application.$controller("MainPageController", ['$rootScope', '$scope', 'Widgets', 'Variables', '$timeout',
    function($rootScope, $scope, Widgets, Variables, $timeout) {
        "use strict";

        var bannerDisplay;

        /** Cancelling the $timeout when page is destroyed*/
        $scope.$on("$destroy", function() {
            $timeout.cancel(bannerDisplay);
        });

        /** For the promotions banner, we have multiple views and we are showing them one after the other
         * with 3000 ms time gap. The below code snippet is used for this, iterating the views and showing them. */
        var views = ['promo-camera', 'promo-mobile', 'promo-peripheral'],
            bannerTimeout = 3000;

        var displayBannerImage = function(index) {
            var i, viewName;
            for (i = 0; i < views.length; i++) {
                viewName = views[i];
                Widgets[viewName].show = (index === i);
            }

            bannerDisplay = $timeout(function() {
                var n = index + 1;
                if (n === views.length) {
                    n = 0;
                }
                displayBannerImage(n);
            }, bannerTimeout);
        };

        /** This is a callback function, that is called after a page is rendered.*/
        $scope.onPageReady = function() {
            $rootScope.pageLoading = false;
            displayBannerImage(0);
        };

        $scope.cameras_listonBeforeUpdate = function(variable, data) {
            WM.forEach(data, function(camera) {
                camera.imgUrl = "http://wmstudio-apps.s3.amazonaws.com/eshop/Products/Thumbnails" + camera.imgUrl;
            });
            return data;
        };

        $scope.smartphones_listonBeforeUpdate = function(variable, data) {
            WM.forEach(data, function(phone) {
                phone.imgUrl = "http://wmstudio-apps.s3.amazonaws.com/eshop/Products/Thumbnails" + phone.imgUrl;
            });
            return data;
        };

        $scope.laptops_listonBeforeUpdate = function(variable, data) {
            WM.forEach(data, function(laptop) {
                laptop.imgUrl = "http://wmstudio-apps.s3.amazonaws.com/eshop/Products/Thumbnails" + laptop.imgUrl;
            });
            return data;
        };

    }
]);


Application.$controller("smartphoneslistController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("cameraslistController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("laptopslistController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);