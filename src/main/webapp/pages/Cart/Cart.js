Application.$controller("CartPageController", ["$scope", "$rootScope", "Widgets", "Variables", "$location",
    function($scope, $rootScope, Widgets, Variables, $location) {
        "use strict";

        $scope.totalPrice = 0;

        /** This is a callback function, that is called after a page is rendered.*/
        $scope.onPageReady = function () {
            /*
             * widgets can be accessed through 'Widgets' service here
             * e.g. Widgets.byId(), Widgets.byName()or access widgets by Widgets.widgetName
             */
            $rootScope.pageLoading = false;
        };

        /**This is a callback function, that is called after the page variables are loaded. */
        $scope.onPageVariablesReady = function () {
            /*
             * variables can be accessed through 'Variables' service here
             * e.g. Variables.staticVariable1.getData()
             */
        };

        $scope.cartonBeforeUpdate = function (variable, data) {
            data.forEach(function (cartItem) {
                cartItem.product.imgUrl = "http://wmstudio-apps.s3.amazonaws.com/eshop/Products/Thumbnails" + cartItem.product.imgUrl;
                $scope.totalPrice += cartItem.product.price;
            });
            if (data.length > 0) {
                Widgets.cartPanel.show = true;
            } else {
                Widgets.no_cart_items_text.show = true;
            }
            return data;
        };

        /*handling cart checkout click. deleting cart data and adding items to orders.*/
        $scope.checkoutClick = function ($event, $scope) {
            $rootScope.pageLoading = true;
            var cartItems = Variables.cart.dataSet.data,
                item = {
                    "user": Variables.currentUser.dataSet,
                    "status": "Ordered"
                };
            /*inserting data to orders table*/
            Variables.call("insertRow", "ordersInsert", {
                "row": item
            }, function (response) {
                WM.forEach(cartItems, function (cartItem) {
                    var item = {
                        "product": cartItem.product,
                        "quantity": cartItem.quantity,
                        "productorder": response
                    };
                    /*inserting data to orderdetails table*/
                    Variables.call("insertRow", "orderDetailsInsert", {
                        "row": item
                    }, function (response) {});
                    /*deleting the cart items from cart table*/
                    Variables.call("deleteRow", "cartDelete", {
                        "id": cartItem.id
                    }, function (response) {});
                });
                /*redirecting to orders page*/
                $location.path("Orders");
            });
        };

    }
]);

Application.$controller("cart-itemsController", ["$scope",
    function ($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);